
'''  bash

git remote add origin https://gitlab.com/sdm_lab1/labexam.git
git config --global user.name "Aashish Gupta"
git config --global user.email "gupta.ashish459@gmail.com"
git push -u origin main

'''

dckr_pat_1ZfBax3HOzmmheJQeTzUSldpt2s

''' bash

docker image build -t ethanracing/myapp .

echo dckr_pat_1ZfBax3HOzmmheJQeTzUSldpt2s | docker login -u ethanracing --password-stdin

docker image push ethanracing/myapp

'''

''' docker
docker service create --name myapp -p 8000:80 --replicas 2 ethanracing/myapp

### swarm
docker swarm init --advertise-addr 10.0.2.15
docker swarm leave --force
docker node ls
docker node inspect <id>
docker service ls


docker service update --image ethanracing/myapp --force myapp


### Alternate to service
docker container run --name myapp -p 8000:80 -itd ethanracing/myapp
docker container rm --force myapp
docker container run --name myapp -p 8000:80 -itd ethanracing/myapp
'''